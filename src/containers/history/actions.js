import { UPDATE_HISTORY, LOAD_SNAPSHOT } from './const';

export const updateHistory = (moveNumber, snapshot, board) => ({
    type: UPDATE_HISTORY,
    moveNumber,
    snapshot,
    board
});

export const loadSnapshot = (snapshot, board) => ({
    type: LOAD_SNAPSHOT,
    snapshot,
    board
});
