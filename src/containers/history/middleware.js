import { getBoard } from '../board/selectors';
import { SET_FIELD_VALUE } from '../game/const';
import { gameStateSelector } from '../game/selectors';
import { updateHistory } from './actions';

export const middleware = (store) => (next) => (action) => {
    next(action);
    if (action.type == SET_FIELD_VALUE) {
        const gameState = gameStateSelector(store.getState());
        const board = getBoard(store.getState());
        store.dispatch(
            updateHistory(gameState.get('moveNumber'), gameState, board)
        );
    }
};
