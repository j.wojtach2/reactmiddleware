import React from 'react';
import { Square } from './square';
import { FIELD_VALUES } from '../../game-logic/const';
import { useDispatch } from 'react-redux';
import { makeMove } from '../game/makeMove';
import { getBoard } from './selectors';
import { useSelector } from 'react-redux';

export const Board = () => {
    const board = useSelector(getBoard);
    const dispatch = useDispatch();
    const onFieldClick = (x, y) => {
        dispatch(makeMove(x, y));
    };
    return (
        <div>
            {board.map((row, y) => (
                <div key={`row-${y}`} className="board-row">
                    {row.map((fieldValue, x) => (
                        <Square
                            key={`field-${x}-${y}`}
                            value={fieldValue}
                            onClick={() => onFieldClick(x, y)}
                            disabled={fieldValue !== FIELD_VALUES.EMPTY}
                        />
                    ))}
                </div>
            ))}
        </div>
    );
};
