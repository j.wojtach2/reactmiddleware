import { prop } from 'ramda';
import { BOARD_REDUCER_NAME } from '../board/reducer';

export const getBoard = prop(BOARD_REDUCER_NAME);
