import { SET_FIELD_VALUE } from '../game/const';
import { fromJS } from 'immutable';
export const BOARD_REDUCER_NAME = 'Board';
import { repeat } from 'ramda';
import { FIELD_VALUES, BOARD_SIZE } from '../../game-logic/const';
import { LOAD_SNAPSHOT } from '../history/const';
const initialBoardState = fromJS(
    repeat(repeat(FIELD_VALUES.EMPTY, BOARD_SIZE), BOARD_SIZE)
);

export const boardReducer = (state = initialBoardState, action) => {
    switch (action.type) {
        case SET_FIELD_VALUE: {
            const { x, y, value } = action;
            return state.setIn([y, x], value);
        }
        case LOAD_SNAPSHOT:
            const { board } = action;
            return board;
        default:
            return state;
    }
};
