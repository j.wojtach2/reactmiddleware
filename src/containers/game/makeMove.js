import { gameStateSelector, isGameFinishedSelector } from './selectors';
import { setFieldValue as setFieldValueAction } from './actions';

export const makeMove = (x, y) => {
    return function makeMoveThunk(dispatch, getState) {
        if (!isGameFinishedSelector(getState())) {
            dispatch(
                setFieldValueAction(
                    x,
                    y,
                    gameStateSelector(getState()).get('currentPlayer')
                )
            );
        }
    };
};
