import { fromJS } from 'immutable';

import { PLAYERS, STARTING_PLAYER } from '../../game-logic/const';

import { SET_FIELD_VALUE } from './const';
import { LOAD_SNAPSHOT } from '../history/const';

export const GAME_REDUCER_NAME = 'Game';

const initialState = fromJS({
    gameState: {
        currentPlayer: STARTING_PLAYER,
        moveNumber: 0
    },
    isRecordedState: false
});

export const gameReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_FIELD_VALUE:
            return state
                .updateIn(['gameState', 'currentPlayer'], (currentPlayer) =>
                    currentPlayer === PLAYERS.X ? PLAYERS.O : PLAYERS.X
                )
                .updateIn(
                    ['gameState', 'moveNumber'],
                    (moveNumber) => moveNumber + 1
                )
                .set('isRecordedState', false);
        case LOAD_SNAPSHOT: {
            const { snapshot } = action;
            return state
                .set('gameState', snapshot)
                .set('isRecordedState', true);
        }
        default:
            return state;
    }
};
