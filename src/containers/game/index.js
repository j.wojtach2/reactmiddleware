import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { Status } from './components';
import { Board } from '../board';
import { History } from '../history';
import {
    isGameFinishedSelector,
    winnnerSelector,
    gameStateSelector
} from './selectors';
import { updateHistory } from '../history/actions';
import { getBoard } from '../board/selectors';

export const Game = () => {
    const gameState = useSelector(gameStateSelector);
    const board = useSelector(getBoard);
    const isGameFinished = useSelector(isGameFinishedSelector);
    const winner = useSelector(winnnerSelector);
    const dispatch = useDispatch();

    // Sending one dispatch at the beginning (to save initial state)
    useEffect(() => {
        console.log(
            updateHistory(gameState.get('moveNumber'), gameState, board)
        );
        dispatch(updateHistory(gameState.get('moveNumber'), gameState, board));
    }, []);

    return (
        <div className="game">
            <div className="game-board">
                <Board />
            </div>
            <div className="game-info">
                <Status
                    isGameFinished={isGameFinished}
                    currentPlayer={gameState.get('currentPlayer')}
                    winner={winner}
                />

                <History />
            </div>
        </div>
    );
};
