import { combineReducers } from 'redux';
import { boardReducer, BOARD_REDUCER_NAME } from './containers/board/reducer';

import { GAME_REDUCER_NAME, gameReducer } from './containers/game/reducer';
import {
    HISTORY_REDUCER_NAME,
    historyReducer
} from './containers/history/reducer';

export default function createReducer() {
    return combineReducers({
        [GAME_REDUCER_NAME]: gameReducer,
        [HISTORY_REDUCER_NAME]: historyReducer,
        [BOARD_REDUCER_NAME]: boardReducer
    });
}
